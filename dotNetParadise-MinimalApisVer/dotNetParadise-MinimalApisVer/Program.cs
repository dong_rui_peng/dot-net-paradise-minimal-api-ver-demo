using Asp.Versioning;
using dotNetParadise_MinimalApisVer.Extensions;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerGen;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddProblemDetails();
builder.Services.AddApiVersioning(options =>
{
    options.DefaultApiVersion = new ApiVersion(2, 0);//默认版本
    options.ReportApiVersions = true;//Response Header 指定可用版本
    options.AssumeDefaultVersionWhenUnspecified = true;//如果没有指定版本用默认配置
}).AddApiExplorer(options =>
{
    options.GroupNameFormat = "'v'VVV";
    options.SubstituteApiVersionInUrl = true;
});
// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();
builder.Services.AddSwaggerGen(options => options.OperationFilter<SwaggerDefaultValues>());

var app = builder.Build();

{
    var todoV1 = app.NewVersionedApi("Todo")
         .HasDeprecatedApiVersion(new ApiVersion(1, 0));//过期版本

    var todoGroup = todoV1.MapGroup("/api/Todo");
    todoGroup.MapGet("/", () => "Version 1.0").WithSummary("请用V2版本代替");
}

{
    var todoV2 = app.NewVersionedApi("Todo")
                         .HasApiVersion(new ApiVersion(2, 0));


    var todoGroup = todoV2.MapGroup("/api/Todo");
    todoGroup.MapGet("/", () => "Version 2.0").MapToApiVersion(new ApiVersion(2, 0)).WithSummary("Version2");


}


{
    var todoV3 = app.NewVersionedApi("Todo")
            .HasApiVersion(new ApiVersion(3, 0));

    var todoGroup = todoV3.MapGroup("/api/Todo");

    todoGroup.MapGet("/", () => "Version 3.0").WithSummary("Version3");

    todoGroup.MapGet("sayhello", (string name) => $"hello {name}").IsApiVersionNeutral();

}


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(
        options =>
        {
            var descriptions = app.DescribeApiVersions();

            // build a swagger endpoint for each discovered API version
            foreach (var description in descriptions)
            {
                var url = $"/swagger/{description.GroupName}/swagger.json";
                var name = description.GroupName.ToUpperInvariant();
                options.SwaggerEndpoint(url, name);
            }
        });
}

app.UseHttpsRedirection();

app.Run();
